//
//  NetworkRequester.swift
//  ApptComTask
//
//  Created by Mina on 9/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit
import Alamofire

class NetworkRequester: NSObject {
    
    func request(method: String, callback: @escaping (_ respons: Result<Any>)->()) {
        
        Alamofire.request(NetworkConstants.baseURL+method).responseJSON { response in
            
            debugPrint(response)
            callback(response.result)
            
        }
    }

}
