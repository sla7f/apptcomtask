//
//  DALAthletes.swift
//  ApptComTask
//
//  Created by Mina on 9/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit

class DALAthletes: NSObject {
    
    func getAthletes(callback: @escaping (_ althletes: [AthleteModel]?) ->()) {
        
        NetworkRequester().request(method: NetworkConstants.getAtheltes) { (responseResult) in
            
            if responseResult.value != nil {
                let athletesModel: AthletesModel  = AthletesModel(fromDictionary: (responseResult.value as! [String : Any]))
                callback(athletesModel.athletes)
            }
            else {
                callback(nil)
            }
        }
    }

}
