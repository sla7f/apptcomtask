//
//  BlReachability.swift
//  ApptComTask
//
//  Created by Mina on 9/16/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Toast_Swift

class BlReachability {
    
    let manager = NetworkReachabilityManager(host: "www.google.com")
    
    func handleNetworkReachability() {
        
        manager?.listener = { status in
            
            print("Network Status Changed: \(status)")
            print("network reachable \(self.manager!.isReachable)")
            
            if !self.manager!.isReachable {
                DispatchQueue.main.async {
                    self.handleTopMostViewController()
                }
            }
        }
        manager?.startListening()
    }
    
    private func handleTopMostViewController() {
        if let topMost  = UIApplication.topViewController() {
            // Print message
            if topMost.isKind(of: LoadingViewController.self) {
                topMost.dismiss(animated: true, completion: {
                    self.handleTopMostViewController()
                })
            }else {
                topMost.view.makeToast("Network is unReachable, Try again!", duration: 3.0 , position: .top)
            }
            
        }

    }
}
