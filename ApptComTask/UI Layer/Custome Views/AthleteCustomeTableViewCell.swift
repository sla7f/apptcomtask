//
//  CustomeTableViewCell.swift
//  ApptComTask
//
//  Created by Mina on 9/13/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit
import Kingfisher

class AthleteCustomeTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var athleteImageView: UIImageView!
    @IBOutlet weak var athleteLabel    : UILabel!
    
    // MARK: - Properties
    

    // MARK: - Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layoutIfNeeded()
        cellSetupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /**
     *  Cell setup data
     */
    func cellSetupData(athleteModel : AthleteModel) {
        // Configure cell data
        self.athleteLabel.text = athleteModel.name
        athleteImageView.kf.indicatorType = .activity
        if let url : URL = URL(string: athleteModel.image) {
            athleteImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "profilePlaceHolder"))
        }
    }
    
    /**
     * Cell setup UI
     */
    func cellSetupUI() {
        athleteImageView.cornerRaduis = athleteImageView.width / 2
    }
    
}
