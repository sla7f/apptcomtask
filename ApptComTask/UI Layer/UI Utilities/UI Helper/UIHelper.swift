//
//  UIHelper.swift
//  ApptComTask
//
//  Created by Mina on 9/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit

class UIHelper: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getLoadingViewController() -> UIViewController {
        return UIStoryboard(name: UIConstants.commonStoryboard, bundle: .main).instantiateViewController(withIdentifier: UIConstants.loadingViewController)
    }
    
    func getAthleteDetailsViewContrroler() -> UIViewController {
        return UIStoryboard(name: UIConstants.mainStoryboard, bundle: .main).instantiateViewController(withIdentifier: UIConstants.athleteDetailsViewController)
    }
    
    

}
