//
//  UIConstants.swift
//  ApptComTask
//
//  Created by Mina on 9/13/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit

class UIConstants {
    
    /**************************************
    ************ Storyboard ***************
    ***************************************/
    
    static let commonStoryboard     = "Common"
    static let mainStoryboard       = "Main"
    
    /***************************************
    ************* View Controller **********
    ****************************************/
    
    static let loadingViewController        = "LoadingViewController"
    static let athleteDetailsViewController = "AthleteDetailsViewController"
    
    /***************************************
     ************** xib View ***************
     ****************************************/
    
    static let athleteCell                  = "AthleteCustomeTableViewCell"
}
