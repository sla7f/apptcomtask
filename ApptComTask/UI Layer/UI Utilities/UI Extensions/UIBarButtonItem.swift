//
//  UIBarButtonItem.swift
//  ApptComTask
//
//  Created by Mina on 9/16/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import UIKit

extension UIBarButtonItem {
    
    func disbale() {
        enabled(enable: false)
    }
    
    func enable() {
        enabled(enable: true)
    }
    
    private func enabled(enable: Bool) {
        self.isEnabled = enable
    }
}
