//
//  AthleteDetailsViewController.swift
//  ApptComTask
//
//  Created by Mina on 9/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit
import Kingfisher

class AthleteDetailsViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var athleteImageView         : UIImageView!
    @IBOutlet weak var athleteNameLabel         : UILabel!
    @IBOutlet weak var athleteDetailsTextView   : UITextView!
    
    // MARK: - Properties
    
    var athleteModel: AthleteModel!
    
    // MARK: - Controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setStyleUI()
        
        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Others
    
    func setStyleUI() {
        athleteImageView.cornerRaduis = athleteImageView.width / 2 
    }
    
    func setData() {
        title = athleteModel.name
        athleteNameLabel.text = athleteModel.name
        athleteDetailsTextView.text = athleteModel.brief
        if let url : URL = URL(string: athleteModel.image) {
            athleteImageView.kf.indicatorType = .activity
            athleteImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "profilePlaceHolder"))
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
