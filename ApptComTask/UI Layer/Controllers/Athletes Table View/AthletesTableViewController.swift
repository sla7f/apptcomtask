//
//  AthletesTableViewController.swift
//  ApptComTask
//
//  Created by Mina on 9/13/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit
import Kingfisher

class AthletesTableViewController: UITableViewController {

    @IBOutlet weak var reloadBarButtonItem: UIBarButtonItem!
    // MARK: - Properties
    
    let cellNameId : String = "AthleteCell"
    var athleteModelsArray : [AthleteModel] = []
    
    // MARK: - Controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // Register custome cell to tableView
        tableView.register(UINib(nibName: UIConstants.athleteCell, bundle: .main), forCellReuseIdentifier: cellNameId)
        
        // Load athletes from api
        getAtheltsFromAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : - Others
    
    func getAtheltsFromAPI() {
        
        // 1 - Disable reloadBarButtonITem
        reloadBarButtonItem.disbale()
        
        // Loading view controller
        let loadingVC = UIHelper().getLoadingViewController()
        self.present(loadingVC, animated: true, completion: nil)
        
        // Call getAthletes api
        DALAthletes().getAthletes { (athletesModelsArr) in
            
            if athletesModelsArr != nil && (athletesModelsArr?.count)! > 0 {
                self.athleteModelsArray = athletesModelsArr!
                self.tableView.reloadData()
            }
            
            if athletesModelsArr != nil && athletesModelsArr?.count == 0 {
                self.tableView.reloadData()
                self.view.makeToast("No Data", duration: 3.0, position: .top)
            }
            
            // Dismiss loading view controller
            loadingVC.dismiss(animated: true, completion: nil)
            
            // Enable reloadBarButtonItme
            self.reloadBarButtonItem.enable()
        }
    }

    // MARK: - Buttons action
    
    /**
     * Reload bar button item action
     */
    @IBAction func reloadBarButtonPressed(_ sender: UIBarButtonItem) {
        athleteModelsArray = []
        getAtheltsFromAPI()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return athleteModelsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AthleteCustomeTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellNameId, for: indexPath) as! AthleteCustomeTableViewCell

        // Configure the cell...
        cell.cellSetupData(athleteModel: athleteModelsArray[indexPath.row])
        
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let athleteDetailsVC : AthleteDetailsViewController = UIHelper().getAthleteDetailsViewContrroler() as! AthleteDetailsViewController
        athleteDetailsVC.athleteModel = athleteModelsArray[indexPath.row]
        navigationController?.pushViewController(athleteDetailsVC, animated: true)
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
