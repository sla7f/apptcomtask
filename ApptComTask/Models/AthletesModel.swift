//
//  AthletesModel.swift
//  ApptComTask
//
//  Created by Mina on 9/13/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit

class AthletesModel: NSObject , NSCoding {
    
    /**
     *  Properties
     */

    var athletes : [AthleteModel]!
    
    private let ATHLETES = "athletes"
    
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        athletes = [AthleteModel]()
        if let athletesArray = dictionary[ATHLETES] as? [[String:Any]]{
            for dic in athletesArray{
                let value = AthleteModel(fromDictionary: dic)
                athletes.append(value)
            }
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if athletes != nil{
            var dictionaryElements = [[String:Any]]()
            for athletesElement in athletes {
                dictionaryElements.append(athletesElement.toDictionary())
            }
            dictionary[ATHLETES] = dictionaryElements
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        athletes = aDecoder.decodeObject(forKey :ATHLETES) as? [AthleteModel]
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if athletes != nil{
            aCoder.encode(athletes, forKey: ATHLETES)
        }
        
    }
}
