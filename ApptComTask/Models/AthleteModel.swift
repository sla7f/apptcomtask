//
//  AthelteModel.swift
//  ApptComTask
//
//  Created by Mina on 9/13/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit

class AthleteModel: NSObject, NSCoding {
    
    /**
     * Properties
     */
    var brief   : String!
    var image   : String!
    var name    : String!
    
    private let BRIEF   = "brief"
    private let IMAGE   = "image"
    private let NAME    = "name"
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        brief = dictionary[BRIEF] as? String
        image = dictionary[IMAGE] as? String
        name = dictionary[NAME] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if brief != nil{
            dictionary[BRIEF] = brief
        }
        if image != nil{
            dictionary[image] = image
        }
        if name != nil{
            dictionary[NAME] = name
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        brief = aDecoder.decodeObject(forKey: BRIEF) as? String
        image = aDecoder.decodeObject(forKey: image) as? String
        name = aDecoder.decodeObject(forKey: NAME) as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if brief != nil{
            aCoder.encode(brief, forKey: BRIEF)
        }
        if image != nil{
            aCoder.encode(image, forKey: image)
        }
        if name != nil{
            aCoder.encode(name, forKey: NAME)
        }
        
    }
    
    
    
}
